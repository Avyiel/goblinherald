GoblinHerald = LibStub("AceAddon-3.0"):NewAddon("GoblinHerald", "AceConsole-3.0", "AceTimer-3.0")
local AceGUI = LibStub("AceGUI-3.0")
local AceConfig = LibStub("AceConfig-3.0")
local AceConfigDialog = LibStub("AceConfigDialog-3.0")

--------------------------------------------------------------------------------------
-- VARIABLES
--------------------------------------------------------------------------------------
local addonName = "GoblinHerald"
local addonTitle = select(2, GetAddOnInfo(addonName))

local frame = AceGUI:Create("Frame")
local ebName = AceGUI:Create("EditBox")
local ebDelay = AceGUI:Create("EditBox")
local ebMessage = AceGUI:Create("EditBox")
local button = AceGUI:Create("Button")
local cbEnable = AceGUI:Create("CheckBox")

local tTimerIds = {}
local testTimer = 0
local testRepeatTimer = 0

local bDebugMode = false
local DEBUG_CHANNEL = "ghdebug"
local DEBUG_CHANNEL_PW = "asimplepassword"

local CHANNEL_ID = 2

-- Default settings
local defaults = {
	global = {
        enabled = true,
        tMessage = {
            id = 0,
            name = "Default",
            msg = "Default message",
            enabled = false,
            delay = 900 -- 15min
        },
        tMessageList = {
            -- Empty list on start
        }
	}
}

-- Chat commands
local tOptions = {
    name = "Goblin Herald",
    handler = GoblinHerald,
    type = "group",
    args = {
        msg = {
            type = 'input',
            name = 'Message',
            desc = 'Message to be sent on repeat',
            set = 'SetMessage',
            get = 'GetMessage',
			order = 90, -- default is 100 so this will put it at the top of non-ordered ones?
        },
        flag1 = {
            type = 'toggle',
            name = 'First flag for my addon',
            desc = 'This can show as a tooltip for the input?',
            set = 'SetFlag1',
            get = 'GetFlag1',
			width = 'full', -- this keeps the checkboxes on one line each
        },
        flag2 = {
            type = 'toggle',
            name = 'Second flag for my addon',
            desc = 'This can show as a tooltip for the input?',
            set = 'SetFlag2',
            get = 'GetFlag2',
			width = 'full',
        },
        rangetest = {
            type = 'range',
            name = 'Range Test',
            desc = 'A range of values - displayed as a slider?',
			min = 10,
			max = 42,
			step = 1,
            set = function(info, val) GoblinHerald.db.profile.rangetest = val end,
            get = function(info) return GoblinHerald.db.profile.rangetest end,
			width = 'double',
			order = 110,
},
    },
}

--------------------------------------------------------------------------------------
-- INITIALIZE
--------------------------------------------------------------------------------------
local optionsTable = AceConfig:RegisterOptionsTable( addonName.."Options", tOptions, {"gh", "gherald", "goblinherald"})

function GoblinHerald:OnInitialize()
    self.db = LibStub("AceDB-3.0"):New(addonName.."DB", defaults)

    self.optionsFrame = AceConfigDialog:AddToBlizOptions( addonName.."Options", addonTitle )

    self:RegisterChatCommand("gh", "onChatCommand")
    self:RegisterChatCommand("gherald", "onChatCommand")
    self:RegisterChatCommand("goblinherald", "onChatCommand")

    self:RegisterChatCommand("ghdebug", "onDebugMode")

    self:StartMainLoop()

    self:DebugMode(bDebugMode)
end

function GoblinHerald:OnEnable()
    -- Called when the addon is enabled
end

function GoblinHerald:OnDisable()
    -- Called when the addon is disabled
end

--------------------------------------------------------------------------------------
-- CHAT FUNCTIONS
--------------------------------------------------------------------------------------
function GoblinHerald:onChatCommand(input)
    local args = {strsplit(" ", input:trim())}
	
	if args[1] == "v" or args[1] == "ver" or args[1] == "version" then
		self:Print(GAME_VERSION_LABEL..": |cd1bd2b00"..GetAddOnMetadata("GoblinHerald","Version").."|r")
    elseif args[1] == "stop" or args[1] == "s" or args[1] == "pause" then
		self:PauseLoop()
        self:Print("Stopping all announcements.")
	elseif args[1] == "resume" or args[1] == "r" or args[1] == "play" or args[1] == "start" then
        self:StartMainLoop()
        self:Print("Restarting all announcements.")
    elseif (args[1] == "test" or args[1] == "t") and (args[2] == "single" or args[2] == "s") then
        testTimer = self:ScheduleTimer("onMessageTimer", 10, "[GoblinHerald] TEST MESSAGE")
    elseif (args[1] == "test" or args[1] == "t") and (args[2] == "repeat" or args[2] == "r") then
        testRepeatTimer = self:ScheduleRepeatingTimer("onMessageTimer", 10, "[GoblinHerald] TEST MESSAGE")
    elseif (args[1] == "test" or args[1] == "t") and args[2] == "stop" then
        self:CancelTimer(testRepeatTimer)
        self:CancelTimer(testTimer)
	else
		self:OpenConfig()
        frame:Hide()
	end
end

function GoblinHerald:onDebugMode(input)
    local args = {strsplit(" ", input:trim())}
    
    if args[1] == "on" then
        bDebugMode = true
        self:Print("Debug mode on")
    elseif args[1] == "off" then
        bDebugMode = false
        self:Print("Debug mode off")
    else
        bDebugMode = not bDebugMode
        self:Print("Debug mode: "..tostring(bDebugMode))
    end

    self:DebugMode(bDebugMode)
end

--------------------------------------------------------------------------------------
-- DEBUG FUNCTIONS
--------------------------------------------------------------------------------------
function GoblinHerald:DebugMode(cond)
    self:CancelAllTimers()

    if cond then
        local cType, cName = JoinChannelByName(DEBUG_CHANNEL, DEBUG_CHANNEL_PW, ChatFrame1:GetID(), nil)
        CHANNEL_ID,_,_ = GetChannelName(DEBUG_CHANNEL)
        self:StartDebugLoop()
    elseif not cond then
        LeaveChannelByName(CHANNEL_ID)
        CHANNEL_ID = 2
        self:StartMainLoop()
    end
end

function GoblinHerald:StartDebugLoop() 
    self.tTimerIds = {}
    self.tTimerIds[1] = self:ScheduleRepeatingTimer("onMessageTimer", 10, "[GoblinHerald] Test chat message")
end

--------------------------------------------------------------------------------------
-- HELPER FUNCTIONS
--------------------------------------------------------------------------------------
function tableSize(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function clone(t, target) -- deep-copy a table 
                          -- target must be {}
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    for k, v in pairs(t) do
        if type(v) == "table" then
            target[k] = clone(v)
        else
            target[k] = v
        end
    end
    setmetatable(target, meta)
end

--------------------------------------------------------------------------------------
-- TIMER FUNCTIONS
--------------------------------------------------------------------------------------
function GoblinHerald:StartMainLoop()
    if not self.db.global.enabled then return end

    self.tTimerIds = {}
    for k,v in ipairs(self.db.global.tMessageList) do
        if v.enabled then
            self.tTimerIds[k] = self:ScheduleRepeatingTimer("onMessageTimer", v.delay, v.message)
            self.db.global.tMessageList.k.id = self.tTimerIds[k]
        end
    end
end

function GoblinHerald:PauseLoop()
    self:CancelAllTimers()
end

function GoblinHerald:onMessageTimer(message)
    SendChatMessage(message, "CHANNEL", nil, CHANNEL_ID)
end

--------------------------------------------------------------------------------------
-- MESSAGE FUNCTIONS
--------------------------------------------------------------------------------------
function GoblinHerald:StageMessage()
    local tStageMsg = {
            id = math.random(200, 2000),
            name = "",
            msg = "",
            enabled = true,
            delay = 900
    }

    tStageMsg.name = ebName:GetText()
    tStageMsg.msg = ebMessage:GetText()
    tStageMsg.enabled = cbEnable:GetValue()
    tStageMsg.delay = ebDelay:GetText()

    GoblinHerald:AddNewMessage(tStageMsg)
    GoblinHerald:Print(tostring(tStageMsg.enabled))
end

function GoblinHerald:AddNewMessage(tMessage)
    local nSize = tableSize(self.db.global.tMessageList)
    self.db.global.tMessageList[nSize] = {}

    clone(tMessage, self.db.global.tMessageList[nSize])
end

function GoblinHerald:RemoveMsgByID(nID)
    for idx,v in ipairs(self.db.global.tMessageList) do
        if self.db.global.tMessageList.idx.id == nID then
            self.db.global.tMessageList.idx = nil
        end
    end
end

function GoblinHerald:RemoveMsgByName(sName)
    for idx,v in ipairs(self.db.global.tMessageList) do
        if self.db.global.tMessageList.idx.name == sName then
            self.db.global.tMessageList.idx = nil
        end
    end
end

function GoblinHerald:EditMessageByID(tMsg)
    for idx,v in ipairs(self.db.global.tMessageList) do
        if self.db.global.tMessageList.idx.id == tMsg.id then
            for k,u in pairs(tMsg) do
                self.db.global.tMessageList.idx.k = u
            end
        end
    end
end

function GoblinHerald:SetMessageText(info, input)
    local nSize = tableSize(self.db.global.tMessageList)

    local tMsg = {}
    clone(self.db.global.tMessage, tMsg)
    tMsg.id = math.random(200,2000)
    tMsg.name = "New Message"
    tMsg.msg = input
    tMsg.enabled = true

    self.db.global.tMessageList[nSize] = {}
    clone(tMsg, self.db.global.tMessageList[nSize])
end

function GoblinHerald:GetMessage(info)
    return self.db.profile.msg
end

function GoblinHerald:SetMessage(info, input)
    self.db.profile.msg = input
end

function GoblinHerald:GetFlag1(info)
    return self.db.profile.flag1
end

function GoblinHerald:SetFlag1(info, input)
    self.db.profile.flag1 = input
end

function GoblinHerald:GetFlag2(info)
    return self.db.profile.flag2
end

function GoblinHerald:SetFlag2(info, input)
    self.db.profile.flag2 = input
end

--------------------------------------------------------------------------------------
-- UI FUNCTIONS
--------------------------------------------------------------------------------------
function GoblinHerald:OpenConfig()
    -- Twice, since the first call only succeeds in opening the options panel itself; the second call opens the correct category
    InterfaceOptionsFrame_OpenToCategory(addonTitle)
    InterfaceOptionsFrame_OpenToCategory(addonTitle)    
end

function createFrame()
    frame:SetTitle("Goblin Herald")
    frame:SetStatusText("Message editor")
    frame:SetCallback("OnClose", function(widget) AceGUI:Release(widget) end)
    frame:SetLayout("Flow")
    frame:SetPoint("CENTER", 0, 0)

    ebName:SetLabel("Name")
    ebName:SetWidth(200)
    frame:AddChild(ebName)

    button:SetText("Save")
    button:SetWidth(80)
    button:SetCallback("OnClick", GoblinHerald:StageMessage())
    frame:AddChild(button)

    ebDelay:SetLabel("Delay")
    ebDelay:SetWidth(60)
    frame:AddChild(ebDelay)

    cbEnable:SetLabel("Enabled")
    cbEnable:SetType("checkbox")
    frame:AddChild(cbEnable)
end
