## Interface: 70300
## Version: 0.1
## Title: Goblin Herald
## Author: Avyiel
## Notes: Configurable announcer for goblins
## SavedVariables: GoblinHeraldDB
## OptionalDeps: Ace3

# Libs
#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua

Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceTimer-3.0\AceTimer-3.0.xml
Libs\AceDB-3.0\AceDB-3.0.xml
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
#@end-no-lib-strip@

GoblinHerald.lua